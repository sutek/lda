clear all;

disp(['reading in topic distributions per document']);
tic
topicDist = textread('model-final.theta', '%s', 'delimiter', '\n');

nDocs = size(topicDist,1);

TopicDistDoc = [];
% first, make the topic distribution into a list of floats for each doc
for n = 1:nDocs
    topicDistributionForDoc = str2double(strsplit(strtrim(topicDist{n}), ' '))';
    TopicDistDoc(n,:) = topicDistributionForDoc;
end

toc
 
%Error using dataread
%Buffer overflow (bufsize = 128000) while reading string from
%file (row 1, field 1). Use 'bufsize' option. See HELP TEXTREAD.
disp(['reading in word distribution over topics']);
tic
wordDist = textread('model-final.phi','%s', 'delimiter', '\n', 'bufsize', 256000);
WordDistTopics = {};

for n = 1:size(wordDist,1)
    values = strsplit(strtrim(wordDist{n}), ' ')';
    WordDistTopics{n,1} = values;
    WordDistTopics{n,2} = n;
end
toc

disp(['determining word mapping']);
tic

wordmap_entries = textread('wordmap.txt', '%s', 'delimiter', '\n');
%word_ids = zeros(size(wordmap_entries,1),2);
nWordCount = str2num(cell2mat((wordmap_entries(1,1)))); 
wordmap_entries(1,:) = [];


for n = 1:size(wordmap_entries,1)
   word_and_count = strsplit(wordmap_entries{n}, ' ');
   %word_ids{n,1} = word_and_count;
   word_ids{n,1} = word_and_count{1};
   word_ids{n,2} = str2num(word_and_count{2});
end
word_ids = sortrows(word_ids, 2);
toc;

% for each document, find the top topic, then find the top words for that
% topic
nTopicsToPrint = 3;
nWordsPerTopic = min(15,nWordCount);
disp(['Writing topic assignments to topics.txt']);
tic
f_output = fopen('topics.txt', 'w');
wordNumbers = (1:nWordCount)';
for i = 1:nDocs
    disp(['Writing topic assignments for document ' num2str(i) '...']);
    [values, top_indices] = sort(TopicDistDoc(i,:), 'descend');
    fprintf(f_output, '\n===================================\nFor document %d', i);
    %[top_topic_value_for_document, top_topic_index_for_document] = max(TopicDistDoc(i,:));

    for j = 1:nTopicsToPrint
        topic_index = top_indices(j);
        
        word_dist = WordDistTopics{topic_index};
        
        %this is expensive! but not as expensive as str2double
        for m = 1:nWordCount
            words_and_ids(m,:) = [sscanf(word_dist{m}, '%f') m];
        end
        %words_and_ids = [str2double(word_dist) wordNumbers];

        top_words_and_ids = sortrows(words_and_ids, -1);        
        top_word_ids = top_words_and_ids(1:nWordsPerTopic,2);
        fprintf(f_output, '\nThe #%d topic is %d (%f)\n', j, topic_index, TopicDistDoc(i,topic_index));
        fprintf(f_output, 'The top words for topic %d are\n', topic_index);

        for k = 1:nWordsPerTopic
            word = word_ids(top_word_ids(k));
            prob = words_and_ids(top_word_ids(k));
            fprintf(f_output, '%s(%f)\n', char(word), prob);
        end

    end

end
fclose(f_output);
toc