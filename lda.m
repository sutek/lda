%note - depends on Barber's brml package for dirrnd - sample from dirichlet
%distribution

clear all
close all



nTopics = 3;
nWordTypes = 20;
nWordsPerDoc = 50;
alphaTopics = 0.1; % Prior for topics
alphaWords = 0.01; % Prior for words
%nSamples = 10;
nDocs = 200;

%dict = ['a'; 'b'; 'c'; 'd'; 'e'; 'f'; 'g'; 'h'; 'j'; 'k'; 'l'; 'm'; 'n'; 'o'; 'p'; 'q'; 'r'; 's'; 't']
dict = 'a':'t';
% Perhaps need to sample instead of just generating one random breakdown?
pi = brml.dirrnd(repmat(alphaTopics, 1, nTopics), nDocs); % Is it really correct to use # of samples to index on? 
                                                          % Otherwise, what is # of samples for?
theta = brml.dirrnd(repmat(alphaWords, 1, nWordTypes), nTopics);

documents = zeros(nDocs, nWordsPerDoc);
%For each doc - 
for doc = 1:nDocs
    % Choose a distribution of topics
    topicDist = pi(:,doc);
    
    % we have a dirichlet distribution for topics, now need to select
    % one... select a uniform random number to select which one
    topicBreaks = zeros(nTopics, 1);
    topicBreaks(1) = topicDist(1);
    for topic = 2:nTopics
        topicBreaks(topic, 1) = sum(topicDist(1:topic)); % cumulative uniform probability
    end
    
    % can we do this up front?
    % Distribute words over topics

    
    for n = 1:nWordsPerDoc
        %Select which topic to use for this word
        rand_value = unifrnd(0,1); % do this separately for debugging
        topic = find(topicBreaks >= rand_value, 1); % need to check this - first or last? I think first is right
        % Should word distribution be regenerated for each document? 
        % I think not
        wordDist = theta(:,topic);
        wordBreaks = zeros(nWordTypes, 1);
        wordBreaks(1) = wordDist(1);
        for m = 2:nWordTypes
            wordBreaks(m, 1) = sum(wordDist(1:m)); % cumulative uniform probability
        end
        
        rand_value = unifrnd(0,1); % do this separately for debugging
        wordIndex = find(wordBreaks >= rand_value, 1); % need to check this - first or last? I think first is right

        word = dict(wordIndex);
        %disp(['producing ' word]);
        documents(doc, n) = char(word);
    end
end

corpusFile = fopen('corpus.txt', 'w');
thetaFile = fopen('theta.txt', 'w');
piFile = fopen('pi.txt', 'w');
fprintf(corpusFile, '%d\n', nDocs);

for doc = 1:nDocs
    fprintf(piFile, 'doc#%d {%f}, {%f}, {%f}\n', doc, pi(1, doc), pi(2,doc), pi(3,doc));
    for word = 1:nWordsPerDoc
        fprintf(corpusFile, '%c ',documents(doc,word));
    end
    fprintf(corpusFile, '\n');
end

for word_type = 1:size(theta,1)
    fprintf(thetaFile, '%c: {%f}, {%f}, {%f}\n', dict(word_type), theta(word_type, 1), theta(word_type, 2), theta(word_type, 3));
end

fclose(corpusFile);
fclose(thetaFile);
fclose(piFile);